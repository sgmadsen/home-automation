#Boilerplate
A node boilerplate with ejs and express

##Functionality & modules

 - Express - fix routing and get/post request super fast
 - EJS rendering - render html pages with javascript widgets
 - default views with some widgets
 - 404, 403, 500 pages
 - project settings
 - Jquery
 - Bootstrap - Css framework
 - node-json-db - Save Json data in textfile
