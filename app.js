var json_db       = require('node-json-db');
var express       = require('express');
var colors        = require('colors');
var util          = require('util');
var crypto        = require('crypto');
var util          = require('util');
var url           = require('url');
var Sequelize     = require('sequelize');
var passport      = require('passport');
var Strategy = require('passport-local').Strategy;
var debug         = require('debug');
var partials      = require('express-partials');
var session       = require('express-session');
var models        = require('./models');

// the express app for the server
var app = express();
// copy express app to standard http server
var server = require('http').Server(app);
// set engine
app.set('view engine', '.ejs');
// Use application-level middleware for common functionality, including
// logging, parsing, and session handling.
app.use(require('morgan')('combined'));
app.use(require('cookie-parser')());
app.use(require('body-parser').urlencoded({ extended: true }));
app.use(require('express-session')({ secret: 'keyboard cat', resave: false, saveUninitialized: false }));

// Initialize Passport and restore authentication state, if any, from the
// session.
app.use(passport.initialize());
app.use(passport.session());

// static folder
app.use('/public', express.static(__dirname + '/public'));
app.set('port', process.env.PORT || 1337);

passport.use(new Strategy(
  function(username, password, cb) {
    models.User.findOne({where: {username: username}}).then(function(user) {
      if (user.password != password) { return cb(null, false); }
      return cb(null, user);
    })
  }));


// Configure Passport authenticated session persistence.
//
// In order to restore authentication state across HTTP requests, Passport needs
// to serialize users into and deserialize users out of the session.  The
// typical implementation of this is as simple as supplying the user ID when
// serializing, and querying the user record by ID from the database when
// deserializing.
passport.serializeUser(function(user, cb) {
  cb(null, user.id);
});

passport.deserializeUser(function(id, cb) {
  models.User.findById(id).then(function(user) {
    cb(null, user);
  })
});

// Define routes.
app.get('/',
  require('connect-ensure-login').ensureLoggedIn(),
  function(req, res) {
    res.render('layout', { user: req.user });
  });

app.get('/login',
  function(req, res){
    res.render('login');
  });

app.get('/create',
  function(req, res){
    res.render('create');
  });

app.post('/login',
  passport.authenticate('local', { failureRedirect: '/login' }),
  function(req, res) {
    res.redirect('/');
  });

app.post('/create',
  function(req, res) {
    models.User.create(req.body)
    .then(function(user) {
      req.user = user;
      res.redirect('/');
    });
  });

app.get('/logout',
  function(req, res){
    req.logout();
    res.redirect('/');
  });

app.get('/profile',
  require('connect-ensure-login').ensureLoggedIn(),
  function(req, res){
    res.render('profile', { user: req.user });
  });


models.sequelize.sync().then(function () {
  var server = app.listen(app.get('port'), function() {
    console.log('Express server listening on port ' + server.address().port);
  });
});

function isAuth(req, res, next) {
    if(req.isAuthenticated){
        next();
    }
    res.redirect('/login');
}